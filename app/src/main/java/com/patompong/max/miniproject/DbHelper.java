package com.patompong.max.miniproject;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DbHelper extends SQLiteOpenHelper {

    private static final String databaseName = "examguide.sqlite";
    private static final int databaseVersion = 1;
    Context myContext;

    public DbHelper(Context context) {
        super(context, databaseName, null, databaseVersion);
        this.myContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        /// สร้าง table exam ///
        String sqltext = "CREATE table examguide ("+
                "examguide_id INTEGER PRIMARY KEY,"+
                "examguide_name TEXT,"+
                "examguide_content TEXT,"+
                "category_id INTEGER"+");";
        db.execSQL(sqltext);

        //// สร้าง category ///
        sqltext = "CREATE table category ("+
                "category_id INTEGER PRIMARY KEY,"+
                "category_name TEXT"+");";
        db.execSQL(sqltext);

        //// INSERT category ///
        sqltext = "INSERT INTO category VALUES (01,'Computer')";
        db.execSQL(sqltext);
        sqltext = "INSERT INTO category VALUES (02,'Business')";
        db.execSQL(sqltext);
        sqltext = "INSERT INTO category VALUES (03,'Engineer')";
        db.execSQL(sqltext);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}

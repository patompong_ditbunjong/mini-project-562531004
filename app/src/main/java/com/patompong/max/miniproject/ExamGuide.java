package com.patompong.max.miniproject;

import android.content.Context;

import java.io.Serializable;


public class ExamGuide implements Serializable{

    private int examguidId;
    private String examguidName;
    private String examguidContent;
    private Category category;



    public int getExamId() {
        return examguidId;
    }

    public void setExamId(int examId) {
        this.examguidId = examId;
    }

    public String getExamName() {
        return examguidName;
    }

    public void setExamName(String examName) {
        this.examguidName = examName;
    }

    public String getExamContent() {
        return examguidContent;
    }

    public void setExamContent(String examContent) {
        this.examguidContent = examContent;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }



}

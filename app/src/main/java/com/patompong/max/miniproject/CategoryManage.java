package com.patompong.max.miniproject;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


public class CategoryManage {

    SQLiteDatabase database;
    DbHelper dbHelper;

    public CategoryManage(Context context) {
        dbHelper = new DbHelper(context);
        database = dbHelper.getWritableDatabase();
    }

    public void Close(){    database.close();   }

    public Category findById(int categoryId){
        Category resultCategory = new Category();
        String sqlText = "SELECT * FROM category WHERE category_id=" + categoryId;
        Cursor cursor = this.database.rawQuery(sqlText, null);
        cursor.moveToFirst();
        if(cursor.getCount() != 0) {
            resultCategory.setCategoryId(cursor.getInt(0));
            resultCategory.setCategoryName(cursor.getString(1));
        }
        return resultCategory;
    }

}

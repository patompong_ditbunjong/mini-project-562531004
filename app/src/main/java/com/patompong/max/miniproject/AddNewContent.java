package com.patompong.max.miniproject;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddNewContent extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_content);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        /*
        FloatIntentgActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */

        final EditText addnew_edittext = (EditText)findViewById(R.id.addnew_edittext);
        final EditText addnew_content = (EditText)findViewById(R.id.addnew_content);
        Button addnew_button = (Button)findViewById(R.id.addnew_button);

        addnew_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExamGuide examguide = new ExamGuide();
                examguide.setExamName(String.valueOf(addnew_edittext.getText()));
                examguide.setExamContent(String.valueOf(addnew_content.getText()));

                ExamGuideManage storybookManage = new ExamGuideManage(getApplicationContext());
                storybookManage.open();
                storybookManage.add(examguide);
                storybookManage.close();
                finish();

            }
        });

    }

}

package com.patompong.max.miniproject;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;


public class NewsListAdapter extends BaseAdapter{

    private static Activity activity;
    private static LayoutInflater layoutInflater;
    private ArrayList<ExamGuide> examguide;

    public NewsListAdapter(Activity activity,ArrayList<ExamGuide> examguide) {
        this.activity = activity;
        this.examguide = examguide;
        this.layoutInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return examguide.size();
    }

    @Override
    public ExamGuide getItem(int position) {
        return examguide.get(position);
    }

    @Override
    public long getItemId(int position) {
        return examguide.get(position).getExamId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        v = layoutInflater.inflate(R.layout.news_layout,null);
        TextView examguideName = (TextView)v.findViewById(R.id.exam_name);
        TextView examguideContent = (TextView)v.findViewById(R.id.exam_content);

        examguideName.setText(examguide.get(position).getExamName());
        examguideContent.setText(examguide.get(position).getExamContent());
        return v;
    }

}

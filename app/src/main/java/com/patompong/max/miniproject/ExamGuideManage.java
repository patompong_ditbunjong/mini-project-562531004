package com.patompong.max.miniproject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;


public class ExamGuideManage {

    SQLiteDatabase database;
    DbHelper dbHelper;
    Context context;

    public ExamGuideManage(Context context){
        dbHelper = new DbHelper(context);
        database = dbHelper.getWritableDatabase();
        this.context = context;
    }

    public void Close(){ database.close(); }

    public ArrayList<ExamGuide> findAll(){
        ArrayList<ExamGuide> resultArrayList = new ArrayList<ExamGuide>();
        String SQLText = "SELECT * FROM examguide ORDER BY examguide_id DESC;";
        Cursor cursor = this.database.rawQuery(SQLText, null);
        cursor.moveToFirst();
        ExamGuide examguide;
        CategoryManage categoryManage = new CategoryManage(this.context);
        while (!cursor.isAfterLast()){
            examguide = new ExamGuide();
            examguide.setExamId(cursor.getInt(0));
            examguide.setExamName(cursor.getString(1));
            examguide.setExamContent(cursor.getString(2));
            examguide.setCategory(categoryManage.findById(cursor.getInt(3)));
            resultArrayList.add(examguide);
            cursor.moveToNext();
        }
        categoryManage.Close();
        return resultArrayList;
    }

    public void add(ExamGuide examguide) {
        ExamGuide newStorybook = new ExamGuide();
        newStorybook = examguide;
        ContentValues values = new ContentValues();
        values.put("examguide_name", newStorybook.getExamName());
        values.put("examguide_content", newStorybook.getExamContent());
        this.database.insert("examguide", null, values);
        Log.d("ExamGuide Demo:::", "Insert Content OK!");
    }

    public void update(ExamGuide examguide) {
        ExamGuide updateExamGuide = examguide;
        ContentValues values = new ContentValues();
        values.put("examguide_name", updateExamGuide.getExamName());
        values.put("examguide_content", updateExamGuide.getExamContent());
        values.put("examguide_id", updateExamGuide.getExamId());
        String where = "examguide_id=" + updateExamGuide.getExamId();

        this.database.update("examguide", values, where, null);
        Log.d("ExamGuide Demo:::", "Update Content OK!");

    }

    public void delete(ExamGuide examguide){
        String sqlText = "DELETE FROM examguide WHERE examguide_id="+examguide.getExamId();
        database.execSQL(sqlText);
    }

    public void open() {
    }

    public void close() {
    }
}

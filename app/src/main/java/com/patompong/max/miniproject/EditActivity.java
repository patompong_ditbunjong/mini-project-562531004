package com.patompong.max.miniproject;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class EditActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        final ExamGuide editExamGuide = (ExamGuide) getIntent().getSerializableExtra("editExamGuide");

        final EditText editText1 = (EditText)findViewById(R.id.editText1);
        final EditText editText2 = (EditText)findViewById(R.id.editText2);
        editText1.setText(editExamGuide.getExamName());
        editText2.setText(editExamGuide.getExamContent());

        Button btnEditSave = (Button)findViewById(R.id.btnEditsave);
        btnEditSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExamGuide eExamGuide = new ExamGuide();
                eExamGuide.setExamId(editExamGuide.getExamId());
                eExamGuide.setExamName(String.valueOf(editText1.getText()));
                eExamGuide.setExamContent(String.valueOf(editText2.getText()));

                ExamGuideManage examguideManage = new ExamGuideManage(getApplicationContext());
                examguideManage.open();
                examguideManage.update(eExamGuide);
                examguideManage.close();
                finish();
            }
        });

        Button btnEditdelete = (Button)findViewById(R.id.btnEditdelete);
        btnEditdelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExamGuide dExamGuide = new ExamGuide();
                dExamGuide.setExamId(editExamGuide.getExamId());
                dExamGuide.setExamName(String.valueOf(editText1.getText()));
                dExamGuide.setExamContent(String.valueOf(editText2.getText()));

                ExamGuideManage examguideManage = new ExamGuideManage(getApplicationContext());
                examguideManage.open();
                examguideManage.delete(dExamGuide);
                examguideManage.close();
                finish();
            }
        });

    }

}

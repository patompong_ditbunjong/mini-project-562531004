package com.patompong.max.miniproject;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.zip.Inflater;


public class MyListView extends BaseAdapter{

    private static Activity activity;
    private static LayoutInflater inflater;
    ArrayList<ExamGuide> myExamGuideList;

    public MyListView(Activity activity, ArrayList<ExamGuide> myExamGuideList) {
        this.myExamGuideList = myExamGuideList;
        this.activity = activity;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return myExamGuideList.size();
    }

    @Override
    public ExamGuide getItem(int position) {
        return myExamGuideList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return myExamGuideList.get(position).getExamId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        v = inflater.inflate(R.layout.my_list_layout, null);
        TextView textView1 = (TextView)v.findViewById(R.id.listview_text);
        TextView textView2 = (TextView)v.findViewById(R.id.listview_content);
        ExamGuide examguide = myExamGuideList.get(position);
        textView1.setText(examguide.getExamName());
        textView2.setText(examguide.getExamContent());
        return v;
    }
}
